# Custom Oracle

Project for custom oracles, running on Google App Engine, integrating with blockchain applications.

# How it will work

Users create their own oracles for integration with their dapps. They select a template
and fill out URLs, regexes, json document locations, etc to pull values. Templates
like a sport template will pull scores for each team and determine when the game has ended.
Other templates could also exist to do different things.

Users will test their integrations against existing pages (i.e. same host, different URL)
so they have confidence it is working correctly.

Perhaps in the future, oracle can pull odds of winning and apply that to dapps. This may be useful
if the game uses play money and the winner is simply the person who makes the most play money.

# Architecture

- TBD, but language will be Python. Most likely front-end will be app engine standard and
backend tasks will be app engine flex with a docker container that can interface with the blockchain.
- Datastore used for saving

# What cryptos will be supported?

- TRX (first, due to cheap transaction costs)
- ETH
- etc

# Can money be made?

I think it makes sense to charge a trivial fee to cover hosting costs and to help compensate
developers for taking their time to make this possible. We could also scrounge together a bounty
system to help expedite work on the project.

Perhaps a TRX token can be generated and devs can earn TRX from the pool by getting rewarded with tokens.

Users could also tip the developers, which would then be split among contributors.

# How can we prevent copy cats from stealing the project if it succeeds?

TBD. Given the borderless nature of cryptos this is pretty hard. Even looking at Tronix, its done a lot of
hard forking as a means of getting to where it is today, which means someone else did the hard work and they reap the
benefit. It's possible after getting a team of devs together we may decide to take this closed source to protect
our IP.
