import flask

app = flask.Flask(__name__)


@app.route("/")
def placeholder():
    return "Placeholder"


if __name__ == "__main__":
    # This is called when running locally only, not in app engine
    app.run(host='127.0.0.1', port=8080, debug=True)
