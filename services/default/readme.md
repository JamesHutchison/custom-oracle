The Default Service
===================

The default service is an un-named python 3.7 standard environment service, 
handling misc. app engine items and responsibilities.

Endpoints use flask.

To run locally:
> python main.py

Note that since this is app engine standard using python 3.7, dev_appserver.py is no longer used.
